import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class FormPage extends StatefulWidget {
  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  var formKey = GlobalKey<FormState>();
  var nameCtrl = TextEditingController();
  var phoneCtrl = TextEditingController();
  var addressCtrl = TextEditingController();
  var name, phone, address;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Form Validation'),
      ),
      body: ListView(
        children: [
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  TextFormField(
                    controller: nameCtrl,
                    decoration: InputDecoration(labelText: 'Enter Name'),
                    validator: (value) {
                      if (value.length == 0) return ("Name is required!");
                    },
                    onSaved: (value) {
                      this.name = value;
                    },
                  ),
                  TextFormField(
                    controller: phoneCtrl,
                    keyboardType: TextInputType.number,
                    maxLength: 11,
                    decoration: InputDecoration(labelText: 'Enter Phone'),
                    validator: (value) {
                      if (value.length == 0) return ("Phone is required!");
                      if (value.length < 11)
                        return ("Phone number must be 11 digit");
                    },
                    onSaved: (value) {
                      this.phone = value;
                    },
                  ),
                  TextFormField(
                    controller: addressCtrl,
                    decoration: InputDecoration(labelText: 'Enter Address'),
                    validator: (value) {
                      if (value.length == 0) return ("Address is required!");
                    },
                    onSaved: (value) {
                      this.address = value;
                    },
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                          child: Text('Submit'), onPressed: submitHandle),
                      SizedBox(
                        height: 20.0,
                      ),
                      ElevatedButton(
                        child: Text('Reset'),
                        onPressed: resetHandle,
                      ),
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  void submitHandle() {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      print("Name is: ${this.name}");
    }
  }

  void resetHandle() {
    nameCtrl.clear();
    phoneCtrl.clear();
    addressCtrl.clear();
  }
}
