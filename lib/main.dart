import 'package:flutter/material.dart';
import 'package:shared_preferance/pages/form.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MaterialApp(
    home: FormPage(),
  ));
}

class Dashboard extends StatefulWidget {
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  var counter = 0;

  init() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    int c = (sp.getInt('counter') ?? 0);
    setState(() {
      this.counter = c;
    });
  }

  increment() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    setState(() {
      this.counter++;
      sp.setInt('counter', this.counter);
    });
  }

  decrement() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    setState(() {
      this.counter--;
      sp.setInt('counter', this.counter);
    });
  }

  @override
  void initState() {
    this.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Dashboard'),
          centerTitle: true,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "$counter",
              style: TextStyle(fontSize: 25),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  child: Text("+"),
                  onPressed: () {
                    increment();
                  },
                ),
                Divider(),
                ElevatedButton(
                  child: Text("-"),
                  onPressed: () {
                    decrement();
                  },
                ),
              ],
            )
          ],
        ));
  }
}
